const PHYSICAL_ITEM_TYPES = new Set([
  'armor',
  'storage',
  'equipment',
  'melee-weapon',
  'ranged-weapon',
  'currency'
] as const)
